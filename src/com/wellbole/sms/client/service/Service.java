/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.service.Service
 * @description: 服务接口
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.service;

/**
 * 服务接口
 * @author dafei
 *
 */
public interface Service {
	/**
	 * 启动服务
	 * @return 启动成功返回true，否则返回false
	 */
	public boolean start();

	/**
	 * 停止服务
	 */
	public void stop();
}
