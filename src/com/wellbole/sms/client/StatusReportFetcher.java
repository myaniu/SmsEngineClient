/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.StatusReportFetcher
 * @description: 状态报告接收接口
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client;

import com.wellbole.sms.client.result.StatusReportFetchResult;

/**
 *
 * 状态报告接收接口
 * @author  李飞
 *
 */
public interface StatusReportFetcher {
	/**
	 * 获取状态报告
	 * @param msgid 消息id，发送时由服务器端返回。
	 * @return 状态报告结果
	 */
	public StatusReportFetchResult fetch(String msgid);
}
