/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.SmsClient
 * @description: 发送数据客户端接口，供其他服务使用
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client;

import java.util.Map;

/**
 * 发送数据客户端接口，供其他服务使用
 * @author 李飞
 */
public interface SmsClient {
	/**
	 * 向服务器发送数据
	 * @param version   应用版本
	 * @param methodName 方法名称
	 * @param parameters 参数列表
	 * @return 服务器端返回的结果
	 */
	public String execute(String version, String methodName, Map<String, String> parameters);
}
