/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.SmsFetcher
 * @description: 短信接受接口
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client;

import com.wellbole.sms.client.result.SmsFetchResult;

/**
 *
 * 短信接收接口
 * @author 李飞
 *
 */
public interface SmsFetcher {
	/**
	 * 短信接收处理
	 * @param extCode 短信扩展码
	 * @return 短信接收结果
	 */
	public SmsFetchResult fetch(String extCode);
}
