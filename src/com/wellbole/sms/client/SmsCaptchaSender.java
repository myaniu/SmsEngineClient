/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.SmsCaptchaSender
 * @description: 短信验证码发送接口
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client;

import com.wellbole.sms.client.result.SmsCaptchaSendResult;

/**
 * 短信验证码发送接口
 * @author 李飞
 */
public interface SmsCaptchaSender {
	/**
	 * 短信验证码发送
	 * @param phone 接收人手机号码
	 * @return 发送结果
	 */
	public SmsCaptchaSendResult SendCaptcha(String phone);
}
