/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.util.SignUtil
 * @description: 签名计算工具类
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.util;

import java.security.MessageDigest;

/**
 *
 * 签名计算工具类
 * @author 李飞
 *
 */
public final class SignUtil {

	/**
	 *不可初始化
	 */
	private SignUtil() {
		// TODO Auto-generated constructor stub
	}

	public static String sha1Hash(String word) {
		String sha1 = "";
		byte[] bytes = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			bytes = md.digest(word.getBytes("UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(bytes != null){
			sha1 = byte2hex(bytes);
		}
		return sha1;
	}

	/**
	 * 二进制转十六进制字符串
	 *
	 * @param bytes
	 * @return
	 */
	private static String byte2hex(byte[] bytes) {
		StringBuilder sign = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(bytes[i] & 0xFF);
			if (hex.length() == 1) {
				sign.append("0");
			}
			sign.append(hex.toUpperCase());
		}
		return sign.toString();
	}

	public static void main(String[] args) {
		System.out.println(SignUtil.sha1Hash("李飞"));
	}
}
