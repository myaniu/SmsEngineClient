/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.impl.DefaultInboundMessageReceivedCallback
 * @description: 默认的上行短信处理回调类。
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.impl;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.wellbole.sms.client.callback.InboundMessageFetchCallback;
import com.wellbole.sms.client.message.InboundMessage;

/**
 * 默认的上行短信处理回调类
 * @author dafei
 *
 */
public class DefaultInboundMessageReceivedCallback implements
		InboundMessageFetchCallback {
	/**
	 * 日志
	 */
	private static final Logger logger = LogManager.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());

	/**
	 * 默认处理函数
	 */
	public void onFetch(InboundMessage inboundMessage) {
		logger.info(inboundMessage.toString());
	}
}
