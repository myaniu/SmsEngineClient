/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.impl.DefaultStatusReportMessageReceivedCallback
 * @description: 默认的状态报告处理回调类。
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.impl;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.wellbole.sms.client.callback.StatusReportMessageFetchCallback;
import com.wellbole.sms.client.message.StatusReportMessage;

/**
 * 默认的状态报告处理回调类。
 * @author 李飞
 *
 */
public class DefaultStatusReportMessageReceivedCallback implements
		StatusReportMessageFetchCallback {
	/**
	 * 日志
	 */
	private static final Logger logger = LogManager.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());

	public void onFetch(StatusReportMessage statusReportMessage) {
		logger.info(statusReportMessage.toString());
	}

}
