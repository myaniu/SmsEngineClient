/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.impl.AbstractOperator
 * @description: 操作服务器抽象基类。
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.wellbole.sms.client.SmsClient;

/**
 *
 * 操作服务器抽象基类
 * @author 李飞
 *
 */
public abstract class AbstractOperator {

	/**
	 * 发送数据客户端接口
	 */
	protected SmsClient client = null;

	/**
	 * 方法名，一般不需要修改
	 */
	protected final String methodName;

	/**
	 * 版本号
	 */
	protected final String version;

	/**
	 * 默认构造函数，什么都不做，主要用于Spring集成
	 * @param version  方法版本
	 * @param methodName 方法签名
	 */
    AbstractOperator(String version, String methodName){
    	this.methodName = methodName;
    	this.version = version;
    }

    /**
     * 构造函数，主要用于编程方式实现的场合
     * @param version  方法版本
	 * @param methodName 方法签名
     * @param client 发送数据客户端接口
     */
    AbstractOperator(String version, String methodName, SmsClient client){
    	this(version, methodName);
    	this.client = client;
    }

    /**
	 * 设定 发送数据客户端接口
	 * @param client 发送数据客户端接口
	 */
	public void setClient(SmsClient client) {
		this.client = client;
	}

	/**
	 * 将类似yyyyMMddHHmmss的字符串转化成Date类型
	 * @param strDate 字符串
	 * @return Date类型/null
	 */
	public Date strToDate(String strDate){
		if(null == strDate || strDate.isEmpty()){
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		try {
			return sdf.parse(strDate);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * 将Date类型转化成类似yyyyMMddHHmmss的字符串
	 * @param date
	 * @return 类似yyyyMMddHHmmss的字符串
	 */
	public String dateToStr(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return sdf.format(date);
	}
}
