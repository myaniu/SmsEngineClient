/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.impl.SmsCaptchaSenderImpl
 * @description: 短信验证码发送器默认实现
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wellbole.sms.client.SmsCaptchaSender;
import com.wellbole.sms.client.SmsClient;
import com.wellbole.sms.client.Version;
import com.wellbole.sms.client.result.SmsCaptchaSendResult;

/**
 *
 * 短信验证码发送器默认实现
 *
 * @author 李飞
 *
 */
public class SmsCaptchaSenderImpl extends AbstractOperator implements
		SmsCaptchaSender {

	/**
	 * 日志
	 */
	private static final Logger logger = LogManager.getLogger(Thread
			.currentThread().getStackTrace()[2].getClassName());

	static{
		logger.info("SmsEngineClient version:"+Version.VERSION + ",update at:" + Version.LAST_UPDATE_AT);
	}

	/**
	 * 默认构造函数
	 */
	public SmsCaptchaSenderImpl() {
		super("1,0", "sms.sendCaptcha");
	}

	/**
	 * 构造函数
	 *
	 * @param client
	 *            发送数据客户端
	 */
	public SmsCaptchaSenderImpl(SmsClient client) {
		super("1,0", "sms.sendCaptcha", client);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.lh.sms.client.SmsCaptchaSender#SendCaptcha(java.lang.String)
	 */
	@SuppressWarnings("static-access")
	public SmsCaptchaSendResult SendCaptcha(String phone) {
		// 限制每秒最多发送100条
		try {
			Thread.currentThread().sleep(20);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// 构造参数表
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("phone", phone);

		// 发送请求，执行结果
		String responseStr = client.execute(version, methodName, parameters);

		// 打印原始值
		logger.debug(responseStr);

		// 根据响应内容构建发送结果
		return createResultFromResponse(responseStr);
	}

	/**
	 * 根据响应内容构建发送结果
	 *
	 * @param responseStr
	 *            执行结果
	 * @return 状态报告接收结果
	 */
	private SmsCaptchaSendResult createResultFromResponse(String responseStr) {
		SmsCaptchaSendResult result = null;
		JSONObject jsonObject = null;
		try{
			jsonObject = JSON.parseObject(responseStr);
		}catch(Exception e){
			result = SmsCaptchaSendResult.createSrrHasError(responseStr);
			return result;
		}
		if(jsonObject.containsKey("code")){
			result = SmsCaptchaSendResult.createSrrHasError(jsonObject.getString("message"));
		}else{
			int resultCode = jsonObject.getInteger("resultCode");
			if (0 == resultCode) {
				result = SmsCaptchaSendResult.createSrrHasData(jsonObject.getString("sha1Captcha"));
			}else{
				result = SmsCaptchaSendResult.createSrrHasError(jsonObject.getString("errorMessage"));
			}
		}
		return result;
	}
}
