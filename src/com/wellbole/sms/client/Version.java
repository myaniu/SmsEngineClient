package com.wellbole.sms.client;

public interface Version {
	public String VERSION = "3.5.0";
	public String LAST_UPDATE_AT="2015-09-08";
}
