/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.callback.InboundMessageFetcherCallback
 * @description: 上行短信接收处理回调接口，当收到上行短信收，系统自动回调实现此接口的类。并执行onReceive方法。
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.callback;

import com.wellbole.sms.client.message.InboundMessage;

/**
 *
 * 上行短信接收处理回调接口，当收到上行短信收，系统自动回调实现此接口的类。并执行onReceive方法
 * @author 李飞
 *
 */
public interface InboundMessageFetchCallback {
	/**
	 * 上行短信回调处理函数
	 * @param inboundMessage 上行短信
	 */
	public void onFetch(InboundMessage inboundMessage);
}