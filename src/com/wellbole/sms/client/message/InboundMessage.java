/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.message.InboundMessage
 * @description: 上行短信消息（手机->平台）
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.message;

import java.util.Date;

/**
 * 上行短信消息（手机->平台）
 * @author  李飞
 *
 */
public final class InboundMessage {
    /**
     * 消息ID，系统自动分配（36位UUID）
     */
    private final String msgid;

    /**
     * 短信的源地址（一般为手机号码，也可能为伪码，CMPP3的版本后改为varchar(36) 以支持伪码）
     */
    private final String phone;

    /**
     * 消息内容
     */
    private final String content;

    /**
     * 接收时间(yyyyMMDDHHMMSS)
     */
    private final Date receiveAt;

    /**
     * 扩展码
     */
    private final String extCode;

    /**
	 * 临时数据，不传输，用于携带关联信息
	 */
	private Object tmpRefData = null;
	

	public InboundMessage(String msgid, String phone, String content, Date receiveAt, String extCode) {
		this.msgid = msgid;
		this.phone = phone;
		this.content = content;
		this.receiveAt = receiveAt;
		this.extCode = extCode;
	}

	/**  
	 * @return msgid 消息ID
	 * @since V1.0.0 
	 */
	public final String getMsgid() {
		return msgid;
	}

	/**  
	 * @return phone 上行手机号码
	 * @since V1.0.0 
	 */
	public final String getPhone() {
		return phone;
	}

	/**  
	 * @return content 上行短信内容
	 * @since V1.0.0 
	 */
	public final String getContent() {
		return content;
	}

	/**  
	 * @return receiveAt 接收时间
	 * @since V1.0.0 
	 */
	public final Date getReceiveAt() {
		return receiveAt;
	}

	/**  
	 * @return extCode 扩展码
	 * @since V1.0.0 
	 */
	public final String getExtCode() {
		return extCode;
	}

	/**  
	 * @return tmpRefData 临时数据，不传输，用于携带关联信息
	 * @since V1.0.0 
	 */
	public final Object getTmpRefData() {
		return tmpRefData;
	}

	/**  
	 * @param tmpRefData  临时数据，不传输，用于携带关联信息  
	 * @since V1.0.0
	 */
	public final void setTmpRefData(Object tmpRefData) {
		this.tmpRefData = tmpRefData;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InboundMessage [msgid=" + msgid + ", phone=" + phone
				+ ", content=" + content + ", receiveAt=" + receiveAt
				+ ", extCode=" + extCode + "]";
	}
}
