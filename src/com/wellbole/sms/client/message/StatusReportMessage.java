/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.message.StatusReportMessage
 * @description: 状态报告消息（手机->平台）
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.message;

import java.util.Date;

/**
 * 状态报告消息（手机->平台）
 * @author  李飞
 *
 */
public final class StatusReportMessage {
	/**
     * 1.发送状态(发送到MAS或者第三方平台成功)
     * 2.发送状态(发送到MAS或者第三方平台失败)
     * 3.发送状态(发送到手机成功)
     * 4.发送状态(发送到手机失败)
     */
	/**
	 * 1.发送状态(发送到MAS或者第三方平台成功)
	 */
	public static final int STATUS_CODE_TO_MAS_OK  = 1;

	/**
	 * 2.发送状态(发送到MAS或者第三方平台失败)
	 */
	public static final int STATUS_CODE_TO_MAS_NG  = 2;

	/**
	 * 3.发送状态(发送到手机成功)
	 */
	public static final int STATUS_CODE_TO_MOBILE_OK  = 3;

	/**
	 * 4.发送状态(发送到手机失败)
	 */
	public static final int STATUS_CODE_TO_MOBILE_NG  = 4;

	/**
     * 消息ID，发送短信时，服务自动返回的串（36位UUID）
     */
    private final String msgid;

    /**
     * 接收人的短信地址（一般为手机号码，也可能为伪码，CMPP3的版本后改为varchar(36) 以支持伪码）
     */
    private final String phone;

    /**
     * 发送时间(yyyyMMDDHHMMSS)
     */
    private final Date sendAt;


    /**
     * 接收时间(yyyyMMDDHHMMSS)
     */
    private final Date receiveAt;

    /**
     * 实际发送条数
     */
    private final int smsCount;

    /**
     * 状态code
     */
    private final int statusCode;

    /**
     * 扩展码
     */
    private final String extCode;
    
    /**
	 * 临时数据，不传输，用于携带关联信息
	 */
	private Object tmpRefData = null;

	
	public StatusReportMessage(String msgid, String phone, Date sendAt, Date receiveAt, int smsCount, int statusCode,
			String extCode) {
		this.msgid = msgid;
		this.phone = phone;
		this.sendAt = sendAt;
		this.receiveAt = receiveAt;
		this.smsCount = smsCount;
		this.statusCode = statusCode;
		this.extCode = extCode;
	}
	
	

	/**  
	 * @return msgid 消息id（发送时设定的）
	 * @since V1.0.0 
	 */
	public final String getMsgid() {
		return msgid;
	}



	/**  
	 * @return phone 手机号码
	 * @since V1.0.0 
	 */
	public final String getPhone() {
		return phone;
	}



	/**  
	 * @return sendAt 发送时间（发送时指定的）
	 * @since V1.0.0 
	 */
	public final Date getSendAt() {
		return sendAt;
	}



	/**  
	 * @return receiveAt 手机接收时间
	 * @since V1.0.0 
	 */
	public final Date getReceiveAt() {
		return receiveAt;
	}



	/**  
	 * @return smsCount 发送短信条数
	 * @since V1.0.0 
	 */
	public final int getSmsCount() {
		return smsCount;
	}



	/**  
	 * @return statusCode 发送状态代码
	 * @since V1.0.0 
	 */
	public final int getStatusCode() {
		return statusCode;
	}



	/**  
	 * @return extCode 扩展吗
	 * @since V1.0.0 
	 */
	public final String getExtCode() {
		return extCode;
	}

	/**  
	 * @return tmpRefData 临时数据，不传输，用于携带关联信息
	 * @since V1.0.0 
	 */
	public final Object getTmpRefData() {
		return tmpRefData;
	}

	/**  
	 * @param tmpRefData  临时数据，不传输，用于携带关联信息  
	 * @since V1.0.0
	 */
	public final void setTmpRefData(Object tmpRefData) {
		this.tmpRefData = tmpRefData;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StatusReportMessage [msgid=" + msgid + ", phone=" + phone
				+ ", statusCode=" + statusCode + ", receiveAt=" + receiveAt
				+ ", sendAt=" + sendAt + ", smsCount=" + smsCount
				+ ", extCode=" + extCode + "]";
	}
}