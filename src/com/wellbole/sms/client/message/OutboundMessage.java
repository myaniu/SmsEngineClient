/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.message.OutboundMessage
 * @description: 下行短信消息（平台->手机）
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.message;

import java.util.Date;

/**
 * 下行短信消息（平台-》手机）
 *
 * @author 李飞
 *
 */
public final class OutboundMessage {
	/**
	 * 目标手机
	 */
	private String phone;

	/**
	 * 短信内容
	 */
	private String content;

	/**
	 * 发送 时间(yyyyMMDDHHMMSS)
	 */
	private Date sendAt = new Date();

	/**
	 * 消息ID，用户可以指定该id，也可以留空，由服务器端返回
	 */
	private String msgid = "";

	/**
	 * 扩展码(0000 ~ 9999)
	 */
	private String extCode = "";
	
	/**
	 *重试次数
	 */
	private int retryCount = 0;
	
	/**
	 * 临时数据，不传输，用于携带关联信息
	 */
	private Object tmpRefData = null;

	/*
	 * @IgnoreSign
	 *
	 * @Pattern(regexp = "\\{Y|N}") private String needReport = "N";
	 */
	
	
	
	
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	
	

	/**  
	 * <p>Title: OutboundMessage</p>  
	 * <p>Description: 默认构造函数</p>  
	 * @since V1.0.0  
	 */
	public OutboundMessage() {
	}

	/**  
	 * <p>Title: OutboundMessage</p>  
	 * <p>Description: 精简构造函数</p>  
	 * @param phone 手机号码
	 * @param content 短信内容
	 * @since V1.0.0  
	 */
	public OutboundMessage(String phone, String content) {
		this.phone = phone;
		this.content = content;
	}
	
	

	/**  
	 * <p>Title: OutboundMessage</p>  
	 * <p>Description: TODO(用一句话描述该构造函数做什么)</p>  
	 * @param phone 手机号码
	 * @param content 短信内容
	 * @param msgid 消息id
	 * @since V1.0.0  
	 */
	public OutboundMessage(String phone, String content, String msgid) {
		this.phone = phone;
		this.content = content;
		this.msgid = msgid;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public OutboundMessage setPhone(String phone) {
		this.phone = phone;
		return this;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public OutboundMessage setContent(String content) {
		this.content = content;
		return this;
	}

	/**
	 * @return the sendAt
	 */
	public Date getSendAt() {
		return sendAt;
	}

	/**
	 * 设定发送时间
	 * @param sendAt 发送时间
	 */
	public OutboundMessage setSendAt(Date sendAt) {
		if (sendAt != null) {
			this.sendAt = sendAt;
		}
		return this;
	}

	/**
	 * @return the extCode
	 */
	public String getExtCode() {
		return extCode;
	}

	/**
	 * @param extCode
	 *            the extCode to set
	 */
	public OutboundMessage setExtCode(String extCode) {
		if (extCode != null) {
			this.extCode = extCode;
		}
		return this;
	}


	/**
	 * @return the msgid 消息ID，用户可以指定该id，也可以留空，由服务器端返回
	 */
	public String getMsgid() {
		return msgid;
	}

	/**
	 * 消息ID，用户可以指定该id，也可以留空，由服务器端返回
	 * @param msgid the msgid to set
	 */
	public OutboundMessage setMsgid(String msgid) {
		if (msgid != null) {
			this.msgid = msgid;
		}
		return this;
	}
	
	public final int getRetryCount() {
		return retryCount;
	}

	public final OutboundMessage setRetryCount(int retryCount) {
		this.retryCount = retryCount;
		return this;
	}
	

	/**  
	 * @return tmpRefData 临时数据，不传输，用于携带关联信息
	 * @since V1.0.0 
	 */
	public final Object getTmpRefData() {
		return tmpRefData;
	}

	/**  
	 * @param tmpRefData  临时数据，不传输，用于携带关联信息  
	 * @since V1.0.0
	 */
	public final OutboundMessage setTmpRefData(Object tmpRefData) {
		this.tmpRefData = tmpRefData;
		return this;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OutboundMessage [phone=").append(phone).append(", content=").append(content).append(", sendAt=")
				.append(sendAt).append(", msgid=").append(msgid).append(", extCode=").append(extCode)
				.append(", retryCount=").append(retryCount).append("]");
		return builder.toString();
	}
}
