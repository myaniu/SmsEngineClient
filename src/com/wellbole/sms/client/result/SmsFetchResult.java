/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.result.SmsFetchResult
 * @description: 上行短信接收结果
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.result;

import java.util.List;

import com.wellbole.sms.client.message.InboundMessage;

/**
 *
 * 上行短信接收结果
 * @author 李飞
 *
 */
public final class SmsFetchResult extends AbstractResult {

	/**
	 * 上行短信列表
	 */
	private final List<InboundMessage> inboundMessageList;

	/**
	 * 构造函数
	 * @param resultCode 结果code
	 * @param errorMessage 错误消息
	 * @param inboundMessageList 上行短信接收结果
	 */
	SmsFetchResult(int resultCode,String errorMessage, List<InboundMessage> inboundMessageList) {
		super(resultCode, errorMessage);
		this.inboundMessageList = inboundMessageList;
	}

	/**
	 * 是否有正常数据返回
	 * @return true,成功返回数据，false，错误或者无数据
	 */
	public final boolean hasData(){
		return this.resultCode == RESULT_OK;
	}

	/**
	 * @return the inboundMessageList
	 */
	public final List<InboundMessage> getInboundMessageList() {
		return inboundMessageList;
	}

	/**
	 * 创建无返回数据的上行短信接收结果
	 * @return 无返回数据的上行短信接收结果
	 */
	public static SmsFetchResult createSrrNoData(){
		return new SmsFetchResult(RESULT_NODATA, "", null);
	}

	/**
	 * 创建发生错误的上行短信接收结果
	 * @param errorMessage 错误信息
	 * @return 发生错误的上行短信接收结果
	 */
	public static SmsFetchResult createSrrHasError(String errorMessage){
		return new SmsFetchResult(RESULT_NG, errorMessage, null);

	}

	/**
	 * 创建有数据的上行短信接收结果
	 * @param statusReportMessageList 上行短信接收结果
	 * @return 有数据的上行短信接收结果
	 */
	public static SmsFetchResult createSrrHasData(List<InboundMessage> inboundMessageList){
		return new SmsFetchResult(RESULT_OK, "", inboundMessageList);
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ReceiveResult [ResultCode= " + this.resultCode
				+ ", ErrorMessage=" + getErrorMessage()
				+ ",inboundMessageList" + inboundMessageList.toString()
				+ "]";
	}
}
