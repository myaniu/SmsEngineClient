/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.result.StatusReportFetchResult
 * @description: 上行状态报告接收结果
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.result;

import com.wellbole.sms.client.message.StatusReportMessage;

/**
 * 上行状态报告接收结果
 * @author 李飞
 *
 */
public final class StatusReportFetchResult extends AbstractResult {
	/**
	 * 上行状态报告列表
	 */
	private final StatusReportMessage statusReportMessage;


	/**
	 * 构造函数
	 * @param resultCode 结果代码
	 * @param errorMessage 错误消息
	 * @param statusReportMessage 上行状态报告
	 */
	StatusReportFetchResult(int resultCode, String errorMessage, StatusReportMessage statusReportMessage) {
		super(resultCode, errorMessage);
		this.statusReportMessage = statusReportMessage;
	}

	/**
	 * 是否有正常数据返回
	 * @return true,成功返回数据，false，错误或者无数据
	 */
	public final boolean hasData(){
		return this.resultCode == RESULT_OK;
	}

	/**
	 * @return the StatusReportMessage
	 */
	public final StatusReportMessage getStatusReportMessage() {
		return statusReportMessage;
	}

	/**
	 * 创建无返回数据的上行状态报告接收结果
	 * @return 无返回数据的上行状态报告接收结果
	 */
	public static StatusReportFetchResult createSrrNoData(){
		return new StatusReportFetchResult(RESULT_NODATA, "", null);
	}

	/**
	 * 创建发生错误的上行状态报告接收结果
	 * @param errorMessage 错误信息
	 * @return 发生错误的上行状态报告接收结果
	 */
	public static StatusReportFetchResult createSrrHasError(String errorMessage){
		return new StatusReportFetchResult(RESULT_NG, errorMessage, null);

	}

	/**
	 * 创建有数据的上行状态报告接收结果
	 * @param statusReportMessageList 上行状态报告接收结果
	 * @return 有数据的上行状态报告接收结果
	 */
	public static StatusReportFetchResult createSrrHasData(StatusReportMessage statusReportMessageList){
		return new StatusReportFetchResult(RESULT_OK, "", statusReportMessageList);
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StatusReportResult [ResultCode= " + this.resultCode
				+ ", ErrorMessage=" + getErrorMessage()
				+ ",inboundMessageList=" + statusReportMessage.toString()
				+ "]";
	}
}
