/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.result.SmsCaptchaSendResult
 * @description: 短信验证码发送结果
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.result;

/**
 *
 * 短信验证码发送结果
 * @author 李飞
 *
 */
public final class SmsCaptchaSendResult extends AbstractResult {
	/**
	 * sha1加密短信验证码后的加密串(服务器端返回)
	 */
	private final String sha1Captcha;

	/**
	 * 构造函数
	 * @param resultCode 结果code
	 * @param errorMessage 错误消息
	 * @param sha1Captcha 短信发送id(服务器端返回)
	 */
	SmsCaptchaSendResult(int resultCode, String errorMessage, String sha1Captcha) {
		super(resultCode, errorMessage);
		this.sha1Captcha = sha1Captcha;
	}

	/**
	 * @return the msgid
	 */
	public final String getSha1Captcha() {
		return sha1Captcha;
	}

	/**
	 * 创建发生错误的短信验证码发送结果
	 * @param errorMessage 错误信息
	 * @return 发生错误的短信验证码发送结果
	 */
	public static SmsCaptchaSendResult createSrrHasError(String errorMessage){
		return new SmsCaptchaSendResult(RESULT_NG, errorMessage, null);

	}

	/**
	 * 创建有数据的短信验证码发送结果
	 * @param sha1Captcha sha1加密短信验证码后的加密串(服务器端返回)
	 * @return 有数据的短信验证码发送结果
	 */
	public static SmsCaptchaSendResult createSrrHasData(String sha1Captcha){
		return new SmsCaptchaSendResult(RESULT_OK, "", sha1Captcha);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SendCaptchaResult [sha1Captcha=" + sha1Captcha + ", resultCode=" + this.resultCode
				+ ", errorMessage=" + this.getErrorMessage() + "]";
	}
}
