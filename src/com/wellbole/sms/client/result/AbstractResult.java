/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.result.AbstractResult
 * @description: 返回结果抽象基类
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.result;

/**
 * 返回结果抽象基类
 * @author 李飞
 *
 */
abstract class AbstractResult {
	/**
	 * 返回结果正常
	 */
	protected final static int RESULT_OK = 0;

	/**
	 * 返回结果异常
	 */
	protected final static int RESULT_NG = 1;

	/**
	 * 无返回结果
	 */
	protected final static int RESULT_NODATA = 2;

	/**
	 * 发送结果状态（0，成功；1，失败；2，无数据）
	 */
	protected final int    resultCode;

	/**
	 * 错误消息，默认为空。
	 */
	private final String errorMessage;

	/**
	 * 构造函数
	 * @param resultCode 结果code
	 * @param errorMessage 错误消息
	 */
	public AbstractResult(int resultCode,String errorMessage) {
		super();
		this.resultCode = resultCode;
		this.errorMessage = errorMessage;
	}

	/**
	 * 是否有错误
	 * @return true有错误，false无错
	 */
	public final boolean hasError(){
		return this.resultCode == RESULT_NG;
	}

	/**
	 * @return 错误信息
	 */
	public final String getErrorMessage() {
		return errorMessage;
	}
}
