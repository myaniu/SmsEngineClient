/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.result.SmsSendResult
 * @description: 短信发送结果
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client.result;

/**
 *
 * 短信发送结果
 * @author 李飞
 *
 */
public final class SmsSendResult extends AbstractResult{

	/**
	 * 短信发送id(服务器端返回)
	 */
	private final String msgid;

	/**
	 * 构造函数
	 * @param resultCode 结果code
	 * @param errorMessage 错误消息
	 * @param msgid 短信发送id(服务器端返回)
	 */
	SmsSendResult(int resultCode, String errorMessage, String msgid) {
		super(resultCode, errorMessage);
		this.msgid = msgid;
	}

	/**
	 * @return the msgid
	 */
	public final String getMsgid() {
		return msgid;
	}

	/**
	 * 创建发生错误的短信发送结果
	 * @param errorMessage 错误信息
	 * @return 发生错误的短信发送结果
	 */
	public static SmsSendResult createSrrHasError(String errorMessage){
		return new SmsSendResult(RESULT_NG, errorMessage, null);

	}

	/**
	 * 创建有数据的短信发送结果
	 * @param msgid 短信发送id(服务器端返回)
	 * @return 有数据的短信发送结果
	 */
	public static SmsSendResult createSrrHasData(String msgid){
		return new SmsSendResult(RESULT_OK, "", msgid);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SendResult [msgid=" + msgid + ", resultCode=" + this.resultCode
				+ ", errorMessage=" + this.getErrorMessage() + "]";
	}
}
