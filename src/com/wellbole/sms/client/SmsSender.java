/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 *
 * @className:com.wellbole.sms.client.SmsSender
 * @description: 短信发送接口
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年7月1日      李飞                       v1.0.0        create
 *
 */
package com.wellbole.sms.client;

import com.wellbole.sms.client.message.OutboundMessage;
import com.wellbole.sms.client.result.SmsSendResult;


/**
 *
 * 短信发送接口
 * @author  李飞
 *
 */
public interface SmsSender {
	/**
	 * 发送短信
	 * @param request 发送请求
	 * @return 发送结果
	 */
	public SmsSendResult send(OutboundMessage outboundMessage);
}
